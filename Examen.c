#include <stdio.h>
int main(void){
    int *pPuntero;
    int var = 10;
    pPuntero = &var;
    printf("[%d]\n", var);
    printf("[%d]\n", *pPuntero);
    var = 20;
    printf("[%d]\n", var);
    printf("[%d]\n", *pPuntero);
    return 0;
}
